// You can also try it on Go Playground - https://go.dev/play/p/xRG7Eq5hVpe
// Make this compile when running `go run primitives3.go`

package main

import "fmt"

func main() {
	var name string
	var emoji rune

	name = "Golang Dojo"
	emoji = "😀"

	// Don't change below this line

	fmt.Println("Value of name and emoji is: ", name, emoji)
}
