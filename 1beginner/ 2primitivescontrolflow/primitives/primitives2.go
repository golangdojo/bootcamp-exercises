// You can also try it on Go Playground - https://go.dev/play/p/CItxSstZr9B
// Make this compile when running `go run primitives2.go`

package main

import "fmt"

func main() {
	var num1 float32 = 10.5
	var num2 float64 = 10.5

	var isEqual bool = num1 == num2

	// Don't change below this line

	if isEqual {
		fmt.Println("num1 and num2 are equal")
	} else {
		fmt.Println("num1 and num2 are not equal")
	}
}
