// You can also try it on Go Playground - https://go.dev/play/p/wDLPHnsy2St
// Make this compile when running `go run primitives1.go`

package main

import "fmt"

func main() {
	var num1 int
	num1 = "10"

	// Don't change below this line

	fmt.Println("Value of num1 is: ", num1)
}
