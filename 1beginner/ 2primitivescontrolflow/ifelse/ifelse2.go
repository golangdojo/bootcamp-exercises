// You can also try it on Go Playground - https://go.dev/play/p/LnQY5IFq6er
// Make this compile when running `go run ifelse2.go`

// The function foo_if_fizz() should 
// return "foo" if "fizz" is passed
// return "bar" if "fuzz" is passed and
// return "baz" otherwise

package main

import "fmt"

func main() {
	fmt.Println(foo_if_fizz("fizz"))
	fmt.Println(foo_if_fizz("fuzz"))
	fmt.Println(foo_if_fizz("literally anything"))
}

// Don't change above this line

func foo_if_fizz(fizzish string) string {
	if fizzish == "fizz" {
		"foo"
	} else {
		1
	}
}
