// You can also try it on Go Playground - https://go.dev/play/p/R6rho9QKZN7
// Make this compile when running `go run ifelse1.go`

package main

import "fmt"

func main() {
	num1, num2 := 12, 11
	fmt.Println("Bigger number between 12 and 11 is: ", bigger(num1, num2))
}

// Complete the below function.

func bigger(num1, num2 int) int {
	// Complete this function to return the bigger number!
	// Do not use:
	// - another function call
	// - additional variables
}
