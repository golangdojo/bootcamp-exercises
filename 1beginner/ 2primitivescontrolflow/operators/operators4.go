// You can also try it on Go Playground - https://go.dev/play/p/FA_I1BTvYjl
// Make this compile when running `go run operators4.go`

package main

import "fmt"

func main() {
	num1 := 10
	num2 := 11

	// Don't change above this line

	if num1 % 2 == 0 & num2 % 2 == 0 {
		fmt.Println("Both numbers are even")
	} else if num1 % 2 == 0 or num2 % 2 == 0 {
		fmt.Println("One of the numbers is even")
	} else {
		fmt.Println("Both numbers are odd")
	}
}
