// You can also try it on Go Playground - https://go.dev/play/p/QslhLNHmSL5
// Make this compile when running `go run operators2.go`

package main

import "fmt"

func main() {
	var num = 15

	// Don't change above this line

	if num % 2 = 0 {
		fmt.Println("Number is even")
	} else {
		fmt.Println("Number is odd")
	}
}
