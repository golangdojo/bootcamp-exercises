// You can also try it on Go Playground - https://go.dev/play/p/hc9bzYUwhfd
// Make this compile when running `go run operators3.go`

package main

import "fmt"

func main() {
	var target int = 10
	var sum int

	// Don't change above this line

	for i := 0; i < target; i+ {
		sum += i
	}

	fmt.Println("Sum of integers till ", target, " is: ", sum)
}
