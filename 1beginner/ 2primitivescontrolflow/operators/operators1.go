// You can also try it on Go Playground - https://go.dev/play/p/RGk8X-oodqL
// Make this compile when running `go run operators1.go`

package main

import "fmt"

func main() {
	var num1, num2 int
	num1 := 10
	num2 := 20

	// Don't change below this line
	
	sum := num1 + num2

	fmt.Println("Sum of ", num1, " and ", num2, " is: ", sum)
}
