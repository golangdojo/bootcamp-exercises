// You can also try it on Go Playground - https://go.dev/play/p/BcLby872fLv
// Make this compile when running `go run comment2.go`

package main

import "fmt"

func main() {
	num := 12
	fmt.Println("12 is even: ", isEven(num))
}

// Don't change above this line

/*
func isEven(num int) bool {
	return num % 2 == 0
}
*/
