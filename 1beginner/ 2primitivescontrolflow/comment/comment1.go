// You can also try it on Go Playground - https://go.dev/play/p/WcVhOcCctTf
// Output should be "Hello World" when running `go run comment1.go`

package main

import "fmt"

func main() {
	// sayHello()
}

// Don't change below this line

func sayHello() {
	fmt.Println("Hello World")
}
