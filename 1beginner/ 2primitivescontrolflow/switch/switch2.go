// You can also try it on Go Playground - https://go.dev/play/p/kpPSw5IPCmw
// Make this compile when running `go run switch2.go`

package main

import "fmt"

func main() {
	switch day = "monday"; day {
	case "monday":	
		fmt.Println("Day is Monday")
	case "tuesday":
		fmt.Println("Day is Tuesday")
	case "wednesday":
		fmt.Println("Day is Wednesday")
	case "thursday":
		fmt.Println("Day is Thursday")
	case "friday":
		fmt.Println("Day is Friday")
	case "saturday":
		fmt.Println("Day is Saturday")
	case "sunday":
		fmt.Println("Day is Sunday")
	default:
		fmt.Println("Invalid day")
	}
}
