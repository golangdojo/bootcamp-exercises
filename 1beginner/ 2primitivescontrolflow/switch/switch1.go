// You can also try it on Go Playground - https://go.dev/play/p/bSxD0ZGgh-n
// Make this compile when running `go run switch1.go`

package main

import "fmt"

func main() {
	var num = 10

	// Don't change above this line

	switch {
	num > 0:	
		fmt.Println("Number is positive")
	case num < 0:
		fmt.Println("Number is negative")
	default:
		fmt.Println("Number is zero")
	}
}
