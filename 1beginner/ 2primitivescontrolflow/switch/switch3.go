// You can also try it on Go Playground - https://go.dev/play/p/aE-VRDuwmV7
// Make this compile when running `go run switch3.go`

// FizzBuzz Problem:
// If num is divisible by 3, print "Fizz"
// If num is divisible by 5, print "Buzz"
// If num is divisible by both 3 and 5, print "FizzBuzz"
// If num is not divisible by 3 or 5, print the number itself

package main

import "fmt"

func main() {
	num := 15

	switch {
	case num % 3 == 0 && num % 5 == 0:
		fmt.Println("FizzBuzz")
		fallthrough
	case num % 3 == 0:
		fmt.Println("Fizz")
	case num % 5 == 0:
		fmt.Println("Buzz")
	default:
		fmt.Println(num)
	}
}
