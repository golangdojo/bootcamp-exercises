// You can also try it on Go Playground - https://go.dev/play/p/BWnf93oTy4_5
// Make this compile when running `go run typedeclaration2.go`

package main

import "fmt"

func main() {
	type Integer int
	var num1 int
	var num2 Integer

	num1 = 10
	num2 = 10

	// Don't change above this line

	if num1 == num2 {
		fmt.Println("num1 and num2 are equal")
	} else {
		fmt.Println("num1 and num2 are not equal")
	}
}
