// You can also try it on Go Playground - https://go.dev/play/p/8mAOtMmRW8b
// Make this compile when running `go run typedeclaration1.go`

package main

import "fmt"

func main() {
	type integer int
	var num1 integer
	var num2 int = 20

	num1 = num2

	// Don't change below this line

	fmt.Println("Value of num1 is: ", num1)
}
