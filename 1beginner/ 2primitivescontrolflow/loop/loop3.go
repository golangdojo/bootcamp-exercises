// You can also try it on Go Playground - https://go.dev/play/p/61fONMJ9FBd
// Make this compile when running `go run loop3.go`

package main

import "fmt"

func main() {
	num := 10
	target := 2
	var sumOfInts int

	// Don't change above this line

	for i = 0 to num; i++ {
		if i % target == 0 {
			sumOfInts += i
		}
	}

	fmt.Println("Sum of integers till ", num, " which is divided by ", target, " is: ", sumOfInts)
}
