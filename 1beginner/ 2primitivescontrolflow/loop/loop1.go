// You can also try it on Go Playground - https://go.dev/play/p/Rxm6JH6W7Ok
// Make this compile when running `go run loop1.go`

package main

import "fmt"

func main() {
	num := 10

	// Don't change above this line

	for i = 0; i < num; i++ {
		fmt.Println(i)
	}
}
