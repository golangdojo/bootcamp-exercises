// You can also try it on Go Playground - https://go.dev/play/p/O40I6jflRNd
// Make this compile when running `go run loop2.go`

package main

import "fmt"

func main() {
	fruits := []string{"apple", "banana", "orange"}

	// Don't change above this line

	for i, fruit = range fruits {
		fmt.Println("Fruit at index", i, "is", fruit)
	}
}
