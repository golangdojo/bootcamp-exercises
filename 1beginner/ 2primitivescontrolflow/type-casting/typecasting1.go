// You can also try it on Go Playground - https://go.dev/play/p/8e4GyzD_8jJ
// Make this compile when running `go run typecasting1.go`

package main

import "fmt"

func main() {
	var num1 int
	var num2 float64

	num1 = 10
	num2 = int(num1)

	// Don't change below this line

	fmt.Println("Value of num1 and num2 is: ", num1, num2)
}
