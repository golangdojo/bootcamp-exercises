// You can also try it on Go Playground - https://go.dev/play/p/QBeSXfkxzEX
// Make this compile when running `go run typecasting2.go`

package main

import "fmt"

func main() {
	name := "Golang Dojo"
	b := byte(name)

	// Don't change below this line

	fmt.Println("Value of b is: ", b)
}
