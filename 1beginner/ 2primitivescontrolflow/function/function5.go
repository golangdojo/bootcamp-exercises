// You can also try it on Go Playground - https://go.dev/play/p/bj1i_l3DxKL
// Make this compile when running `go run function5.go`

package main

import "fmt"

func main() {
	answer := square(3)
	fmt.Println("The square of 3 is: ", answer)
}

// Don't change above this line

func square(num int) int {
	num * num
}
