// You can also try it on Go Playground - https://go.dev/play/p/rrdtVzUm8Jj
// Make this compile when running `go run function3.go`

package main

import "fmt"

func main() {
	callMe()
}

// Don't change below
func callMe(num int) {
	for i := 0; i < num; i++ {
		fmt.Println("Ring! Call number ", i + 1)
	}
}
