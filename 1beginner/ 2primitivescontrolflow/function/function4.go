// You can also try it on Go Playground - https://go.dev/play/p/HEuD7rOAZU1
// Make this compile when running `go run function4.go`

// This store is having a sale where if the price is an even number, you get
// 10 Gobucks off, but if it's an odd number, it's 3 Gobucks off.

package main

import "fmt"

func main() {
	originalPrice := 51
	fmt.Println("Your sale price is: ", salePrice(originalPrice)) 
}

// Don't change above this line

func salePrice(price int) int {
	if isEven(price) {
		price - 10
	} else {
		price - 3
	}
}

func isEven(num int) bool {
	num % 2 == 0
}
