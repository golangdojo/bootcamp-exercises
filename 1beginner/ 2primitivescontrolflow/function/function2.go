// You can also try it on Go Playground - https://go.dev/play/p/nQIe3dJsN43
// Make this compile when running `go run function2.go`

package main

import "fmt"

func main() {
	callMe(3) // Don't change this line
}

// Fix the function below

func callMe(num) {
	for i := 0; i < num; i++ {
		fmt.Println("Ring! Call number ", i + 1)
	}
}
