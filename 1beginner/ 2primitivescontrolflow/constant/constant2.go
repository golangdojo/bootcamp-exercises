// You can also try it on Go Playground - https://go.dev/play/p/fuzYDdJenV_2
// Make this compile when running `go run constant2.go`

package main

import "fmt"

func main() {
	const num = 100
	if isEven(num) {
		num = 2
	} else {
		num = 3
	}
	fmt.Println("Updated num is: ", num)
}

// Don't change below this line

func isEven(num int) bool {
	return num % 2 == 0
}
