// You can also try it on Go Playground - https://go.dev/play/p/I0XsKhtwjTJ
// Make this compile when running `go run constant1.go`

package main

import "fmt"

func main() {
	const price = 100
	fmt.Println("The price is: ", price)

	// Don't change above this line

	price = 20
	fmt.Println("The price is: ", price)
}
