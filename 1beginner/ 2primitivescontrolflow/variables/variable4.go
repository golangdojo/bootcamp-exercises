// You can also try it on Go Playground - https://go.dev/play/p/4CoaCk-CqTK
// Make this compile when running `go run variable4.go`

package main

import "fmt"

func main() {

	name, age = "John", 32

	// Don't change below this line

	fmt.Println("Your name is: ", name, " and your age is: ", age)
}
