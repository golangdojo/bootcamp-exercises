// You can also try it on Go Playground - https://go.dev/play/p/wrnDCfPffEK
// Make this compile when running `go run variable2.go`

// Make x equal to 10

package main

import "fmt"

func main() {
	var x

	// Don't change below this line

	if x == 10 {
		fmt.Println("x is ten!")
	} else {
		fmt.Println("x is not ten!")
	}
}
