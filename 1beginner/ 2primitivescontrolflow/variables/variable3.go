// You can also try it on Go Playground - https://go.dev/play/p/7QemGWbhFzm
// Make this compile when running `go run variable3.go`

// Assign your age to the age variable

package main

import "fmt"

func main() {
	var age int

	// Don't change below this line

	fmt.Println("Your age is: ", age)
}
