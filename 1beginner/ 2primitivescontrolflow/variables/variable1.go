// You can also try it on Go Playground - https://go.dev/play/p/pfbZ6h84bpK
// Make this compile when running `go run variable1.go`

package main

import "fmt"

func main() {
	x = 5

	// Don't change below this line

	fmt.Println("x has the value: ", x)
}
