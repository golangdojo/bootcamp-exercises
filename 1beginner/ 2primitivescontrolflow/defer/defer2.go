// You can also try it on Go Playground - https://go.dev/play/p/8wijyYi7EPK
// When running `go run defer2.go`, it should output the result and then exit.

package main

import (
	"fmt"
	"os"
)

func main() {
	num := 105
	exit()
	if isEven(num) {
		fmt.Println("Number is even")
	} else {
		fmt.Println("Number is odd")
	}
}

// Don't change below this line

func isEven(num int) bool {
	return num%2 == 0
}

func exit() {
	fmt.Println("Exiting the program")
	os.Exit(0)
}
