// You can also try it on Go Playground - https://go.dev/play/p/w_O-j4yEXYL
// Output should be "Hello World" when running `go run defer1.go`

package main

import "fmt"

func main() {

	// Fix the below code

	defer fmt.Print("Hello ")
	fmt.Print("World\n")

}
