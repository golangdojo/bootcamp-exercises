// You can also try it on Go Playground - https://go.dev/play/p/Lup225maHBW
// When running `go run defer3.go`, The content of the file should be "Hello World!"
// Use defer keyword to fix the below code.

package main

import (
	"fmt"
	"os"
)

func main() {
	file, _ := os.Create("first.txt")
	file.Close()
	fmt.Fprintln(file, "Hello World!")
}
